package 
{
	import air.update.ApplicationUpdater;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filesystem.FileStream;
	import flash.utils.ByteArray;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.net.URLRequest;
	
	/**
	 * MovieClipRenderer, v. 1.0.0
	 * 
	 * This program allows easy rendering of MovieClips into PNGs, as comissioned by MhX.
	 * 
	 * @version 1.0.0 
	 * @author Asynchronee
	 */
	public class Main extends Sprite 
	{
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point - add any code you'd like to be run automatically below this comment
			 
			// =============== SAMPLE USAGE BELOW ===============
			/*
			// Create a MovieClip to hold our image
			var foo:MovieClip = new MovieClip();
			addChild(foo);
			 
			// Load an image to the MovieClip (obviously, this can be omitted if the MovieClip is already parsed)
			var bar:Loader = new Loader();
			bar.contentLoaderInfo.addEventListener(Event.COMPLETE, function()
			{
				foo.addChild(bar);
				Render(foo, "foobar.png");
			});
			bar.load(new URLRequest("http://battleversia.com/media/gfx/SmallLogo.png"));
			*/
		}
				
		public function Render(mc:MovieClip, name:String):void 
		{
			// Create bitmap to store graphical data
			var data:BitmapData = new BitmapData(mc.width, mc.height);
			
			// Draw MovieClip to bitmap
			data.draw(mc);
			
			// Convert bitmap into PNG byte sequence
			var bytes:ByteArray = PNGEncoder.encode(data);
			
			// Check if MCR directory already exists, if not, create if
			var mcrCheck:File = File.desktopDirectory.resolvePath("MCR");
			
			trace(mcrCheck.exists);
			
			if (!mcrCheck.exists)
			{
				mcrCheck.createDirectory();
			}
			
			// Create destination file		
			var file:File = File.desktopDirectory.resolvePath("MCR/" + name);
			
			// Open destination file and write data to it
			var stream:FileStream = new FileStream();
			stream.open(file, FileMode.WRITE);
			stream.writeBytes(bytes, 0, bytes.length);
			stream.close();
		}
	}
	
}